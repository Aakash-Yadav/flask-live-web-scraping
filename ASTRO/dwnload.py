from bs4 import BeautifulSoup
from requests import get


class screaping(object):


    def __init__(self) -> None:
        self.link = 'https://gogoanime.pe/'
        # self.html_doc = get(self.link).text
        # self.bs4_data = BeautifulSoup(self.html_doc,'lxml')


    def get_html(self, link):
        return BeautifulSoup(get(link).text, 'lxml')


    def Search_Anime(self, name: str):
        name = name.replace(' ', '%20').lower()
        return f'https://gogoanime.pe//search.html?keyword={name}'


    def get_all_name_last_episodes(self, name):
        soup = self.get_html(self.Search_Anime(name))
        'last_episodes'
        self.next_link = []
        self.image_link = []
        self.name_ofanime = []
        self.relese_data = []


        for value in soup.find('div', {'class': 'last_episodes'}).find_all('li'):

            self.next_link.append(
                (value.find('div', {'class': 'img'}).find('a')['href'].split('/')[-1]))

            self.image_link.append(
                (value.find('div', {'class': 'img'}).find('img')['src']))
            self.name_ofanime.append(value.find(
            'p', {'class': 'name'}).find('a').text)

            self.relese_data.append(value.find(
            'p', {'class': 'released'}).text)
        return (self.next_link, self.image_link, self.name_ofanime, self.relese_data)


    def get_epsodes_links(self, data):
        self.data = data
        self.main_link = 'https://gogoanime.pe//category/'+self.data
        soup_scond_page = self.get_html(self.main_link)
        image = soup_scond_page.find(
            'div', {'class': 'anime_info_body_bg'}).find('img')['src']
        print(image)
        summary = soup_scond_page.find_all('p', class_='type')[1].text


        soup_ep = soup_scond_page.find('div', {'id': 'load_ep'})

        ems = (soup_scond_page.find('div', class_='anime_video_body').find(
        'a', class_='active').text).split('-')[-1]

        return summary ,image,int(ems)


    def main_screep(self,url,num):
        'anime_info_body_bg'
        suop = self.get_html(f'https://gogoanime.pe/{url}-episode-{num}')
        # data =(suop.find('div',{'class':'play-video'}).find('iframe')['src'].split('/')[-1].split('?'))
        data =(suop.find('div',{'class':'play-video'}).find('iframe')['src'].split('/')[-1].split('?')[-1].split('id=')[-1])
        return data
    

    def get_make_data(self,name,num):
        suop = self.get_html(f'https://streamani.io/download?id={self.main_screep(name,num)}')
        data = suop.find('div',{'class':'content_c_bg'})
        info_data_of_anime={'data':data.find('div',{'class':'sumer_l'}).text,
        'links':data.find_all('div',{'class':'dowload'})}
        link  = []
        name = []
        for i in (info_data_of_anime['links']):
            link.append(str(i.find('a')['href']))
            name.append(str(i.find('a').text))
        return name,link

'D_Cide Traumerei the Animation' 
print(screaping().get_all_name_last_episodes('D_Cide Traumerei the Animation' ))