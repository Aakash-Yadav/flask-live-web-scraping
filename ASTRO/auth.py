from flask import Blueprint,render_template,request,redirect,flash
from requests.api import get
from .dwnload import screaping
auth = Blueprint('auth', __name__)


@auth.route('/Download',methods=['GET','POST'])
def Download():
    fail = False
    if request.method == 'POST':
        n = request.form.get('Anime')
        data = screaping().get_all_name_last_episodes(str(n))
        name = data[0]
        if len(name)==0:
            fail = True
            flash(f'{n} not found by Astro Boi',category='error')
            return render_template('from.html',fail=fail)     
        zip_data = zip(data[0],data[1],data[2],data[3])
        return render_template('dwnload.html',zip_data=zip_data)
    return render_template('from.html',fail=fail)



@auth.route('/Downloads/<n>')
def Dwnloads(n:str):
    global nu
    nu = n.split('/')[-1]
    data = screaping().get_epsodes_links(nu)
    image = data[0]
    summary = data[1]
    total_ep = range(1,data[-1]+1)
    return render_template('news.html',image=image,summary=summary,total_ep=total_ep)


@auth.route('/MAIN/<n>')
def main(n):
    data_ = screaping().get_make_data(str(nu),str(n))
    return render_template('get_dow.html',data_=zip(data_[0],data_[-1]))



@auth.route('/News')
def News():
    return render_template('news.html')