from flask  import Flask , Blueprint 
from .auth import auth
from .views import views
from os import urandom

def making_app():
    app = Flask(__name__)
    app.register_blueprint(auth, url_prefix='/')
    app.register_blueprint(views, url_prefix='/')
    app.config['SECRET_KEY'] = f'{urandom(50)}'
    return app
